#!/bin/bash


declare options=("awesome
bash
bspwm
dunst
dwm
i3
neovim
qutebrowser
st
sxhkd
xinitrc
xmobar
xmonad
xresources
quit")

choice=$(echo -e "${options[@]}" | dmenu -p 'Edit config file: ')

case "$choice" in
	quit)
		echo "Program terminated." && exit 1
	;;
	awesome)
		choice="$HOME/.config/awesome/rc.lua"
	;;
	bash)
		choice="$HOME/.bashrc"
	;;
	bspwm)
		choice="$HOME/.config/bspwm/bspwmrc"
	;;
	dunst)
		choice="$HOME/.config/dunst/dunstrc"
	;;
	dwm)
		choice="$HOME/dwm-6.2/config.h"
	;;
	i3)
		choice="$HOME/.config/i3/config"
	;;
	neovim)
		choice="$HOME/.config/nvim/init.vim"
	;;
	qutebrowser)
		choice="$HOME/.config/qutebrowser/config.py"
	;;
	st)
		choice="$HOME/st-0.8.4/config.h"
	;;
	sxhkd)
		choice="$HOME/.config/sxhkd/sxhkdrc"
	;;
	xinitrc)
		choice="$HOME/.xinitrc"
	;;
	xmobar)
		choice="$HOME/.config/xmobar/xmobarrc2"
	;;
	xmonad)
		choice="$HOME/.xmonad/xmonad.hs"
	;;
	xresources)
		choice="$HOME/.Xresources"
	;;
	*)
		exit 1
	;;
esac
st -e nvim "$choice"
# emacsclient -c -a emacs "$choice"

