#!/bin/bash

wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/UbuntuMono.zip

unzip UbuntuMono.zip

mkdir -p ~/.local/share/fonts/nerdfonts/ubuntumono

mv *.ttf ~/.local/share/fonts/nerdfonts/ubuntumono

fc-cache -f -v
