#!/bin/bash

wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/Mononoki.zip

unzip Mononoki.zip

mkdir -p ~/.local/share/fonts/nerdfonts/mononoki

mv *.ttf ~/.local/share/fonts/nerdfonts/mononoki

fc-cache -f -v
