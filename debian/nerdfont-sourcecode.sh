#!/bin/bash

wget https://github.com/ryanoasis/nerd-fonts/releases/download/v2.1.0/SourceCodePro.zip

unzip SourceCodePro.zip

mkdir -p ~/.local/share/fonts/nerdfonts/scp

mv *.ttf ~/.local/share/fonts/nerdfonts/scp

fc-cache -f -v
